# README #

Installation Requirements

* Java 6+
* Linux Bluez Library (http://www.bluez.org/)
* Sqlite3 (apt-get install sqlite3)
* Hcidump (apt-get install bluez-hcidump)
* Common Libraries (apt-get install libusb-dev libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev)

### What is this repository for? ###

* Main ATLAS Gateway
* Version 0.0.8
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Quick Run.
Clone the application for GIT repo, Run gradle bootRun

* Configuration

Add the startup/gateway script to the /etc/init.d/
update-rc.d gateway defaults
Then reboot, the gateway will start at boot time.

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact