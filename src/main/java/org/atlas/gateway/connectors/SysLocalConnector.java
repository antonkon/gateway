package org.atlas.gateway.connectors;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.atlas.wsn.exceptions.WsnException;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SysLocalConnector {

	private static final Logger logger = LoggerFactory.getLogger(SysLocalConnector.class);
	
	private static AtlasConnection localConnection;
	private static MqttConnectOptions connectionOptions;
	private static boolean isConnected;
	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	
	static{
		localConnection = AtlasConnectionFactory.createConnection("tcp://127.0.0.1:1883", "Local-Client-Publisher");
		localConnection.setListener(null);
		connectionOptions = new MqttConnectOptions();
		connectionOptions.setUserName("application");
		connectionOptions.setPassword("@PpL3c@tI0n".toCharArray());
		connectionOptions.setAutomaticReconnect(true);
		connectionOptions.setCleanSession(true);
		
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {	
				isConnected = localConnection.checkConnection();
				logger.info("Checking local MQTT connection --> Connected: " + isConnected);
				if( !isConnected ) isConnected = doConnect();
			}
		}, 1, 10, TimeUnit.SECONDS);
		
	}
	
	private static boolean doConnect(){
		localConnection.connect(connectionOptions);
		return localConnection.checkConnection();
	}

	/**
	 * Publish message to the Applications
	 * @param topic - Topic
	 * @param payload - Payload
	 */
	public static void publish(String topic, byte[] payload, int qos){
		if( isConnected ) localConnection.publish(topic, payload, qos);
	}
	
}
