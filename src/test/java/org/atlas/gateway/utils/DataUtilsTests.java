package org.atlas.gateway.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.atlas.gateway.utils.IpAddrUtils;
import org.atlas.gateway.utils.OsUtils;
import org.junit.Test;

public class DataUtilsTests {
	
	@Test
	public void checkIntegerToHexConverterText(){
		assertEquals(DataUtils.rateToHex(500, "ti"), "32");
		assertEquals(DataUtils.rateToHex(1000, "ti"), "64");
		
		System.out.println(DataUtils.transformDataToNumeric("humidity", "F4 5E 30 6B", ""));
		System.out.println(DataUtils.transformDataToNumeric("light", "6E 18", ""));
		System.out.println(DataUtils.transformDataToNumeric("barometer", "F4 5E 30 6B 8B 01", ""));
		System.out.println(DataUtils.transformDataToNumeric("temperature", "80 06 20 0A", ""));
		
	}
	
}
