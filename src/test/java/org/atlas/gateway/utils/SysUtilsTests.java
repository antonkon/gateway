package org.atlas.gateway.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.atlas.gateway.utils.IpAddrUtils;
import org.atlas.gateway.utils.OsUtils;
import org.junit.Test;

public class SysUtilsTests {
	
	@Test
	public void checkMacTransformationTest(){
		assertEquals(SysUtils.stringifyMacAddress("4C:BB:58:DA:60:1D"), "4CBB58DA601D");
	}
	
}
